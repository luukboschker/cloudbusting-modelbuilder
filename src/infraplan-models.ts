export interface InfraplanItem {
    id: string;
    parentId: string;
    sourceId: string;
    targetId: string;
    element: {
        type: 'resource' | 'connection' | 'label',
        businessObject: any;
    };
}

export interface Infraplan {
  elements: Array<InfraplanItem>;
}
