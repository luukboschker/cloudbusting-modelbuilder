import { Infraplan, InfraplanItem } from './infraplan-models';
import {
  BusinessObject, IncomingConnection, OutgoingConnection, Resource
} from './model';

export class ModelBuilder {
  private readonly infraplan: Infraplan;

  constructor(infraplan: Infraplan) {
    this.infraplan = infraplan;
  }

  /**
   * Transform the infraplan into a denormalized model that can be used by Cloudbusters.
   *
   * @returns the denormalized infraplan model. It contains the settings and relations
   * (both parent resources, incoming and outgoing connections). See the readme.MD file for more
   * information.
   */
  build(): Array<Resource> {
    return this.infraplan
      .elements
      .filter((item) => item.element.type === 'resource')
      .map((resource) => ({
        ...resource.element.businessObject,
        parent: this.createParent(resource),
        children: this.createChildren(resource),
        incoming: this.createIncomingConnections(resource),
        outgoing: this.createOutgoingConnections(resource)
      }));
  }

  private createParent(resource: InfraplanItem): BusinessObject | undefined {
    const parent = this.findResource(resource.parentId);

    return {
      ...parent?.element.businessObject
    };
  }

  private createChildren(resource: InfraplanItem): Array<BusinessObject> {
    return this.infraplan
      .elements
      .filter((item) => item.element.type === 'resource' && item.parentId === resource.id)
      .map((child) => ({
        ...child.element.businessObject
      }));
  }

  private createIncomingConnections(resource: InfraplanItem): Array<IncomingConnection> {
    return this.infraplan
      .elements
      .filter((item) => item.element.type === 'connection')
      .filter((connection) => connection.targetId === resource.id)
      .map((connection) => {
        const incomingResource = this.findResource(connection.sourceId);

        return {
          ...connection.element.businessObject,
          from: {
            ...incomingResource?.element.businessObject
          }
        };
      });
  }

  private createOutgoingConnections(resource: InfraplanItem): Array<OutgoingConnection> {
    return this.infraplan
      .elements
      .filter((item) => item.element.type === 'connection')
      .filter((connection) => connection.sourceId === resource.id)
      .map((connection) => {
        const outgoingResource = this.findResource(connection.targetId);

        return {
          ...connection.element.businessObject,
          to: {
            ...outgoingResource?.element.businessObject
          }
        };
      });
  }

  private findResource(id: string): InfraplanItem | undefined {
    return this.infraplan.elements.find((item) => item.id === id);
  }
}
