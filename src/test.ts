import * as fs from 'fs';
import { Infraplan } from './infraplan-models';
import { ModelBuilder } from './model-builder';

const infraplan: Infraplan = JSON.parse(fs.readFileSync('./example/aws-demo.json', 'utf-8'));

const model = new ModelBuilder(infraplan);
const resources = model.build();

const output = JSON.stringify(resources, undefined, 2);

const outputFile = './example/output.json';
if (fs.existsSync(outputFile)) {
  fs.unlinkSync(outputFile);
}

fs.writeFileSync(outputFile, output);
