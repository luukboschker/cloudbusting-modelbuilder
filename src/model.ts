export interface BusinessObject {
  name: string;
  resourceId: string;
  resourceType: string;
  [others: string]: unknown;
}

export interface IncomingConnection extends BusinessObject {
  from: BusinessObject;
}

export interface OutgoingConnection extends BusinessObject {
  to: BusinessObject;
}

export interface Resource extends BusinessObject {
    parent?: BusinessObject;
    children: Array<BusinessObject>;
    incoming: Array<IncomingConnection>;
    outgoing: Array<OutgoingConnection>;
}
