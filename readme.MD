# Cloudbusting model builder

The cloudbusting model builder is a part of https://cloudbusting.it.

It transforms infraplan files created by cloudbusting.it into a model that can be used by a Cloudbuster to create artifacts for your cloud infrastructure.

First install the modelbuilder:

```
npm install @cloudbusting/modelbuilder
```

Then, in your cloudbuster, read and parse the infraplan JSON and create a new ModelBuilder object with it.

```javascript
const fs = require('fs');
const modelbuilder = require('@cloudbusting/modelbuilder').ModelBuilder;

const file = fs.readFileSync('infraplans/aws-demo.json');
const infraplan = JSON.parse(file);

const model = modelbuilder.build(infraplan);

const resources = model.build();
```

This creates a denormalized version of the infraplan, which is easy to use as input for Cloudbusters.

The model is an array with each resource in the infraplan. The item has the following structure:

```javascript
{
    "resourceId": "",       // Unique ID for this resource
    "resourceType": "",
    "name": "",
    "setting1": "",         // Settings and values as defined in the blueprint
    "settingN": "", 
    "parent": {             // If applicable, the item's parent item
      "resourceId": "",
      "resourceType": "",
      "name": "",
      "setting1": "",
      "settingN": "", 
    },
    {
      "children": [         // All items contained in this resource 
        {
          "resourceId": "",
          "resourceType": "",
          "name": "",
          "setting1": "",
          "settingN": ""
        }
      ]
    },
    "incoming": [           // Array with all items that are connected to this item
      {                   
        "name": "",
        "resourceId": "",   // Of the connection itself
        "resourceType": "", 
        "from": {           // Properties of the item the connection is coming from
          "resourceId": "",
          "resourceType": "connection",
          "name": "",
          "setting1": ""
          "settingN": "", 
        }
      }
    ],
    "outgoing": [           // Array with all items this item is connected to
      {                   
        "name": "",
        "resourceId": "",   
        "resourceType": "connection", 
        "from": {           // Properties of the item the connection is going to
          "resourceId": "",
          "resourceType": "",
          "name": "",
          "setting1": ""
        }
      }
    ]
}
```
